# Copyright (c) 2016, DWANGO Co., Ltd.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

bl_info = {
    "name": "Freehand Curve Drawing",
    "author": "ideasman42",
    "version": (0, 1),
    "blender": (2, 77, 0),
    "location": "View3d -> Options -> Curve Options, Shift-LMB to Draw",
    "description": "Edit-mode curve sketching",
    "warning": "",
    "wiki_url": "",
    "category": "Add Curve",
    }

# Self contained Python3 module to calculate bezier curves from an array of points.
# This script is in 2 parts, first the module (no Blender deps), then the addon
# (could be split out, keeping a single file for now).

USE_ADDON = True

# ----------------------------------------------------------------------------
# points_to_bezier_3d.py

from collections import namedtuple
import math

# constants
DBL_MAX = 1.79769e+308
DBL_NAN = float("nan")

__all__ = (
    "calc_spline",
    )

CornerPoint = namedtuple(
        'CornerPoint',
        ('point',
         'original_index',
         'sharpness',
         'is_corner',
         ))


Cubic = namedtuple(
        'Cubic',
        ('p0',
         'p1',
         'p2',
         'p3',
         ))


# Typing:
try:
    import typing
except ImportError:
    typing = None

if typing is not None:
    from typing import (
            List,
            Sequence,
            Tuple,
            )
else:
    # Stub out typing for pypy

    Sequence = {None: None, Cubic: None, int: None, float: None}
    Vector = None
    Vector = Sequence[float]
    List = {None: None, float: None, int: None, Cubic: None}
    Tuple = {None: None, (float, int): None}

Vector = Sequence[float]


# ----------------------------------------------------------------------------
# Mini Math Lib (using tuples)

def len_vnvn(v0, v1):
    return math.sqrt(len_squared_vnvn(v0, v1))


def len_vn(v0):
    return math.sqrt(len_squared_vn(v0))


def len_squared_vnvn(v0, v1):
    d = sub_vnvn(v0, v1)
    return len_squared_vn(d)


def len_squared_vn(v0):
    return dot_vnvn(v0, v0)


def normalized_vn(v0):
    d = len_squared_vn(v0)
    if d != 0.0:
        d = math.sqrt(d)
        return tuple(a / d for a in v0)
    else:
        return tuple(0.0 for a in v0)


def normalized_vnvn(v0, v1):
    return normalized_vn(sub_vnvn(v0, v1))


def mul_vn_fl(v0, f):
    return tuple(a * f for a in v0)


def dot_vnvn(v0, v1):
    return sum(a * b for a, b in zip(v0, v1))


def add_vnvn(v0, v1):
    return tuple(a + b for a, b in zip(v0, v1))


def sub_vnvn(v0, v1):
    return tuple(a - b for a, b in zip(v0, v1))


def negated_vn(v0):
    return tuple(-a for a in v0)


def add_vn_index(v0: Vector, i: int, value: float) -> Vector:
    return v0[:i] + ((v0[i] + value),) + v0[i + 1:]

# End math lib
# ----------------------------------------------------------------------------


def cubic_evaluate(cubic: Cubic, t: float) -> Vector:
    s = 1.0 - t

    p01 = add_vnvn(mul_vn_fl(cubic.p0, s), mul_vn_fl(cubic.p1, t))
    p12 = add_vnvn(mul_vn_fl(cubic.p1, s), mul_vn_fl(cubic.p2, t))
    p23 = add_vnvn(mul_vn_fl(cubic.p2, s), mul_vn_fl(cubic.p3, t))

    # now reuse the variables already used

    p01 = add_vnvn(mul_vn_fl(p01, s), mul_vn_fl(p12,  t))   # l2
    p23 = add_vnvn(mul_vn_fl(p12,  s), mul_vn_fl(p23, t))  # r1

    # l3-r0
    p12 = add_vnvn(mul_vn_fl(p01, s), mul_vn_fl(p23, t))
    return p12


def cubic_calc_point(cubic: Cubic, t: float) -> Vector:
    s = 1.0 - t
    # return cubic.p0 * s * s * s + 3.0 * t * s * (s * cubic.p1 + t * cubic.p2) + t * t * t * cubic.p3
    t3 = t * t * t
    s3 = s * s * s
    return tuple(p0 * s3 + 3.0 * t * s * (s * p1 + t * p2) + t3 * p3
                 for p0, p1, p2, p3 in zip(cubic.p0, cubic.p1, cubic.p2, cubic.p3))


def cubic_calc_speed(cubic: Cubic, t: float) -> Vector:
    s = 1.0 - t
    # return 3.0 * ((cubic.p1 - cubic.p0) * s * s + 2.0 * (cubic.p2 - cubic.p0) * s * t + (cubic.p3 - cubic.p2) * t * t)
    t2 = t * t
    s2 = s * s
    return tuple(3.0 * ((p1 - p0) * s2 + 2.0 * (p2 - p0) * s * t + (p3 - p2) * t2)
                 for p0, p1, p2, p3 in zip(cubic.p0, cubic.p1, cubic.p2, cubic.p3))


def cubic_calc_acceleration(cubic: Cubic, t: float) -> Vector:
    s = 1.0 - t
    # return 6.0 * ((cubic.p2 - 2.0 * cubic.p1 + cubic.p0) * s + (cubic.p3 - 2.0 * cubic.p2 + cubic.p1) * t)
    return tuple(6.0 * ((p2 - 2.0 * p1 + p0) * s + (p3 - 2.0 * p2 + p1) * t)
                 for p0, p1, p2, p3 in zip(cubic.p0, cubic.p1, cubic.p2, cubic.p3))


def cubic_calc_error(
        cubic: Cubic,
        points_offset: Sequence[Vector],
        points_offset_len: int,
        u: float
        ) -> Tuple[float, int]:

    '''
    Returns a 'measure' of the maximal discrepancy of the points specified
    by points_offset from the corresponding cubic(u[]) points.
    '''

    error_sq_max = 0.0
    error_index = 0

    for i in range(1, points_offset_len - 1):
        pt_eval = cubic_evaluate(cubic, u[i])
        pt_real = points_offset[i]

        err_sq = len_squared_vnvn(pt_real, pt_eval)
        if err_sq >= error_sq_max:
            error_sq_max = err_sq
            error_index = i
    return error_sq_max, error_index


def sq(a):
    return a * a


def B1(u: float) -> float:
    tmp = 1.0 - u
    return 3.0 * u * tmp * tmp


def B2(u: float) -> float:
    return 3.0 * u * u * (1.0 - u)


def B0plusB1(u: float) -> float:
    tmp = 1.0 - u
    return tmp * tmp * (1.0 + 2.0 * u)


def B2plusB3(u: float) -> float:
    return u * u * (3.0 - 2.0 * u)


def is_almost_zero(val, eps=1e-8):
    return -eps < val and val < eps


def points_calc_center_weighted(
        points_offset: Sequence[Vector],
        points_offset_len: int,
        ) -> Vector:
    """
    Calculate a center that compensates for point spacing.
    """

    w_tot = 0.0
    center = (0.0,) * len(points_offset[0])

    i_prev = points_offset_len - 2
    i_curr = points_offset_len - 1

    w_prev = len_vnvn(points_offset[i_prev], points_offset[i_curr])

    for i_next in range(points_offset_len):
        w_next = len_vnvn(points_offset[i_curr], points_offset[i_next])

        w = w_prev + w_next
        w_tot += w

        center = add_vnvn(center, mul_vn_fl(points_offset[i_curr], w))

        i_prev = i_curr
        i_curr = i_next

        w_prev = w_next

    if w_tot != 0.0:
        center = mul_vn_fl(center, 1.0 / w_tot)

    return center


def cubic_from_points(
        points_offset: Sequence[Vector],
        points_offset_len: int,
        u_prime: Vector,
        tan_l: Vector,
        tan_r: Vector,
        ) -> Cubic:

    p0 = points_offset[0]
    p3 = points_offset[points_offset_len - 1]

    # double X[2], C[2][2]
    X = [0.0, 0.0]
    C = [[0, 0], [0, 0]]

    for i in range(points_offset_len):
        A = (mul_vn_fl(tan_l, B1(u_prime[i])),
             mul_vn_fl(tan_r, B2(u_prime[i])))

        C[0][0] += dot_vnvn(A[0], A[0])
        C[0][1] += dot_vnvn(A[0], A[1])
        C[1][1] += dot_vnvn(A[1], A[1])

        C[1][0] = C[0][1]

        b0_plus_b1 = B0plusB1(u_prime[i])
        b2_plus_b3 = B2plusB3(u_prime[i])
        tmp = tuple((p_axis - (p0[j] * b0_plus_b1)) + (p3[j] * b2_plus_b3)
                    for j, p_axis in enumerate(points_offset[i]))
        X[0] += dot_vnvn(A[0], tmp)
        X[1] += dot_vnvn(A[1], tmp)

    # double's
    detC0C1 = C[0][0] * C[1][1] - C[0][1] * C[1][0]
    detC0X = X[1] * C[0][0] - X[0] * C[0][1]
    detXC1 = X[0] * C[1][1] - X[1] * C[0][1]

    if is_almost_zero(detC0C1):
        detC0C1 = C[0][0] * C[1][1] * 10e-12

    alpha_l = detXC1 / detC0C1
    alpha_r = detC0X / detC0C1

    # The problem that the stupid values for alpha dare not put only when we realize
    # that the sign and wrong, but even if the values are too high.
    # But how do you evaluate it?

    # Meanwhile, we should ensure that 'These values are sometimes so' only problems absurd
    # of approximation and not for bugs in the code.

    if (alpha_l < 0.0 or alpha_r < 0.0):
        alpha_l = alpha_r = len_vnvn(p0, p3) / 3.0

    p1 = sub_vnvn(p0, mul_vn_fl(tan_l, alpha_l))
    p2 = add_vnvn(p3, mul_vn_fl(tan_r, alpha_r))

    # ------------------------------------
    # Clamping (we could make it optional)
    center = points_calc_center_weighted(points_offset, points_offset_len)
    clamp_scale = 3.0  # clamp to 3x

    dist_sq_max = 0.0
    for i_curr in range(points_offset_len):
        # dist_sq_max = max(dist_sq_max, len_squared_vnvn(center, points_offset[i_curr]))

        # Use a scaled distance instead
        ofs = sub_vnvn(points_offset[i_curr], center)
        ofs = mul_vn_fl(ofs, clamp_scale)
        dist_sq_max = max(dist_sq_max, len_squared_vn(ofs))
        del ofs
    # print(dist_sq_max, len_squared_vnvn(center, p1), len_squared_vnvn(center, p2))
    p1_dist_sq = len_squared_vnvn(center, p1)
    p2_dist_sq = len_squared_vnvn(center, p2)
    if (p1_dist_sq > dist_sq_max or
            p2_dist_sq > dist_sq_max):

        alpha_l = alpha_r = len_vnvn(p0, p3) / 3.0
        p1 = sub_vnvn(p0, mul_vn_fl(tan_l, alpha_l))
        p2 = add_vnvn(p3, mul_vn_fl(tan_r, alpha_r))

        p1_dist_sq = len_squared_vnvn(center, p1)
        p2_dist_sq = len_squared_vnvn(center, p2)
        # print("Clamp")

    # clamp within the 3x radius
    if p1_dist_sq > dist_sq_max:
        p1 = sub_vnvn(p1, center)
        p1 = mul_vn_fl(p1, math.sqrt(dist_sq_max) / math.sqrt(p1_dist_sq))
        p1 = add_vnvn(p1, center)
    if p2_dist_sq > dist_sq_max:
        p2 = sub_vnvn(p2, center)
        p2 = mul_vn_fl(p2, math.sqrt(dist_sq_max) / math.sqrt(p2_dist_sq))
        p2 = add_vnvn(p2, center)

    del center, dist_sq_max, p1_dist_sq, p2_dist_sq

    # end clamping
    # ------------

    cubic = Cubic(
            # TThickPoint ? - vector is fine too
            tuple(p0),
            tuple(p1),
            tuple(p2),
            tuple(p3),
            )

    return cubic


def points_calc_coord_length(
        points_offset: Sequence[Vector],
        points_offset_len: int,
        ) -> List[float]:

    u = [None] * points_offset_len
    u[0] = 0.0
    for i in range(1, points_offset_len):
        u[i] = u[i - 1] + len_vnvn(points_offset[i], points_offset[i - 1])
    # assert(!is_almost_zero(u[points_offset_len - 1]))
    w = u[points_offset_len - 1]
    for i in range(1, points_offset_len):
        u[i] = u[i] / w
    return u


# ----------------------------------------------------------------------------
#
def cubic_find_root(
        cubic: Cubic,
        p: Vector,
        u: float
        ) -> float:
    """
    Newton-Raphson Method.
    """
    # all vectors
    q0U = cubic_calc_point(cubic, u)
    q1U = cubic_calc_speed(cubic, u)
    q2U = cubic_calc_acceleration(cubic, u)

    # return u - ((q0U - p) * q1U) / (q1U.length_squared() + (q0U - p) * q2U)
    try:
        return u - dot_vnvn(sub_vnvn(q0U, p), q1U) / (len_squared_vn(q1U) + dot_vnvn(sub_vnvn(q0U, p), q2U))
    except ZeroDivisionError:
        return DBL_NAN


def cubic_reparameterize(
        cubic: Cubic,
        points_offset: Sequence[Vector],
        points_offset_len: int,
        u: Vector,
        ) -> List[float]:
    """
    Recalculate the values of u[] based on the Newton Raphson method
    """

    # double *u_prime = new double[points_offset_len]
    u_prime = [None] * points_offset_len

    for i in range(points_offset_len):
        u_prime[i] = cubic_find_root(cubic, points_offset[i], u[i])
        if not math.isfinite(u_prime[i]):
            del u_prime  # free
            return None

    u_prime.sort()

    if u_prime[0] < 0.0 or u_prime[points_offset_len - 1] > 1.0:
        del u_prime
        return None

    assert(u_prime[0] >= 0.0)
    assert(u_prime[points_offset_len - 1] <= 1.0)

    return u_prime

# ----------------------------------------------------------------------------


def fit_cubic_to_points(
        # Points
        points_offset: Sequence[Vector],
        points_offset_len: int,
        tan_l: Vector,
        tan_r: Vector,
        error_threshold: float,
        # fill in this value
        r_cubic_array: Sequence[Cubic],
        ):

    iteration_max = 4  # const (make configurable?)
    error_sq = sq(error_threshold)

    if points_offset_len == 2:
        dist = len_vnvn(points_offset[0], points_offset[1]) / 3.0
        r_cubic_array.append(
                Cubic(points_offset[0],
                      sub_vnvn(points_offset[0], (mul_vn_fl(tan_l, dist))),
                      add_vnvn(points_offset[1], (mul_vn_fl(tan_r, dist))),
                      points_offset[1],
                      )
                )
        return

    u = points_calc_coord_length(points_offset, points_offset_len)

    cubic = cubic_from_points(points_offset, points_offset_len, u, tan_l, tan_r)

    error_sq_max, split_index = cubic_calc_error(cubic, points_offset, points_offset_len, u)

    if (error_sq_max < error_sq):
        del u
        r_cubic_array.append(cubic)
        return

    else:
        u_prime = None
        for i in range(iteration_max):
            # delete u_prime
            u_prime = cubic_reparameterize(cubic, points_offset, points_offset_len, u)
            if u_prime is None:
                break

            del cubic
            cubic = cubic_from_points(points_offset, points_offset_len, u_prime, tan_l, tan_r)
            error_sq_max, split_index = cubic_calc_error(cubic, points_offset, points_offset_len, u_prime)
            if error_sq_max < error_sq:
                del u_prime  # free
                del u  # free
                r_cubic_array.append(cubic)
                return

            del u  # free
            u = u_prime

    del u  # free
    del cubic  # free

    # XXX check splinePoint is not an endpoint???
    #
    # This assert happens sometimes... Look into it but disable for now. campbell!
    # assert(split_index > 1)
    pt_a = points_offset[split_index - 1]
    pt_b = points_offset[split_index + 1]
    pt = points_offset[split_index]


    assert(split_index < points_offset_len)

    if pt_a == pt_b:
        pt_a = points_offset[split_index]

    tan_center_a = normalized_vnvn(pt_a, pt)
    tan_center_b = normalized_vnvn(pt, pt_b)
    tan_center = normalized_vn(add_vnvn(tan_center_a, tan_center_b))

    fit_cubic_to_points(
            points_offset, split_index + 1,
            tan_l, tan_center, error_threshold, r_cubic_array)
    fit_cubic_to_points(
            points_offset[split_index:], points_offset_len - split_index,
            tan_center, tan_r, error_threshold, r_cubic_array)


# ----------------------------------------------------------------------------
# Simple corner detection

def angle_vnvnvn(v0, v1, v2):
    a = normalized_vnvn(v0, v1)
    b = normalized_vnvn(v1, v2)
    d = dot_vnvn(a, b)
    # sanity check
    d = max(-1.0, min(1.0, d))
    return math.acos(d)


def isect_line_sphere_vn(
        l1: Vector,
        l2: Vector,
        sp: Vector,
        r: float
        ) -> Vector:

    ldir = sub_vnvn(l2, l1)

    a = len_squared_vn(ldir)
    b = 2.0 * dot_vnvn(ldir, sub_vnvn(l1, sp))
    c = len_squared_vn(sp) + len_squared_vn(l1) - (2.0 * dot_vnvn(sp, l1)) - (r * r)

    i = b * b - 4.0 * a * c

    if i < 0.0 or a == 0.0:
        # no intersections
        return None
    elif i == 0.0:
        # one intersection
        mu = -b / (2.0 * a)
        return add_vnvn(l1, mul_vn_fl(ldir, mu))
    elif i > 0.0:
        i_sqrt = math.sqrt(i)  # avoid calc twice

        # first intersection

        # Note: when l1 is inside the sphere and l2 is outside.
        # the first intersection point will always be between the pair.
        mu = (-b + i_sqrt) / (2.0 * a)
        return add_vnvn(l1, mul_vn_fl(ldir, mu))

        '''
        # second intersection
        mu = (-b - i_sqrt) / (2.0 * a)
        return add_vnvn(l1, mul_vn_fl(ldir, mu))
        '''
    else:
        return None


def point_corner_measure(
        points,
        i,
        i_prev, i_next,
        radius,
        samples_max,
        ):
    p = points[i]
    i_prev_init = i_prev
    i_prev_next = i_prev + 1
    while True:
        if i_prev == -1 or ((i_prev_init - i_prev) > samples_max):
            return None, None, -1, -1
        elif len_squared_vnvn(p, points[i_prev]) < radius:
            i_prev -= 1
        else:
            break

    i_next_init = i_next
    i_next_prev = i_next - 1
    while True:
        if i_next == len(points) or ((i_next - i_next_init) > samples_max):
            return None, None, -1, -1
        elif len_squared_vnvn(p, points[i_next]) < radius:
            i_next += 1
        else:
            break

    # find points on the sphere
    p_prev = isect_line_sphere_vn(
            points[i_prev], points[i_prev_next],
            p, radius)
    if p_prev is None:
        return None, None, -1, -1

    p_next = isect_line_sphere_vn(
            points[i_next], points[i_next_prev],
            p, radius)
    if p_next is None:
        return None, None, -1, -1

    return (
        p_prev, p_next,
        i_prev_next, i_next_prev,
        )


def point_corner_angle(
        points, i,
        radius_min,  # ignore values below this
        radius_max,  # ignore values above this
        angle_limit,
        # prevent locking up when for example `radius_min` is very large
        # (possibly larger then the curve).
        # In this case we would end up checking every point from every other point,
        # never reaching one that was outside the `radius_min`.
        samples_max,  # prevent locking up when for e
        ):

    # Use the difference in angle between the mid-max radii
    # to detect the difference between a corner and a sharp turn.
    radius_mid = (radius_min + radius_max) / 2.0

    p = points[i]

    (p_mid_prev, p_mid_next,
     i_mid_prev_next, i_mid_next_prev,
     ) = point_corner_measure(
            points, i,
            i - 1,
            i + 1,
            radius_mid,
            samples_max,
            )

    if p_mid_prev is not None and p_mid_next is not None:
        angle_mid = angle_vnvnvn(p_mid_prev, p, p_mid_next)
        if angle_mid > angle_limit:
            (p_max_prev, p_max_next,
             i_max_prev_next, i_max_next_prev,
             ) = point_corner_measure(
                    points, i,
                    i_mid_prev_next,
                    i_mid_next_prev,
                    radius_max,
                    samples_max,
                    )
            if p_max_prev is not None and p_max_next is not None:
                angle_max = angle_vnvnvn(p_max_prev, p, p_max_next) / 2
                angle_diff = angle_mid - angle_max
                if angle_diff > angle_limit:
                    return angle_diff

    return 0.0


def corners_detect(
        points,
        corners,
        *,
        radius_min,  # ignore values below this
        radius_max,  # ignore values above this
        samples_max,
        angle_limit):

    # we could ignore first/last- but simple to keep aligned with the point array
    angle_limits = [
            point_corner_angle(points, i, radius_min, radius_max, angle_limit, samples_max)
            for i in range(0, len(points))
            ]

    # Clean angle limits!
    #
    # How this works:
    # - Find contiguous 'corners' (where the distance is less or equal to the error threshold).
    # - Keep track of the corner with the highest angle
    # - Clear every other angle (so they're ignored when setting corners).
    if 1:
        radius_min_sq = sq(radius_min)
        i_span_start = 0
        while i_span_start < len(points):
            i_span_end = i_span_start
            if angle_limits[i_span_start] != 0.0:
                i_next = i_span_start + 1
                i_best = i_span_start
                while i_next < len(points):
                    if ((angle_limits[i_next] == 0.0) or
                       (len_squared_vnvn(points[i_next - 1], points[i_next]) > radius_min_sq)):
                        break
                    else:
                        if angle_limits[i_best] < angle_limits[i_next]:
                            i_best = i_next
                        i_span_end = i_next
                        i_next += 1

                if i_span_start != i_span_end:
                    i = i_span_start
                    while i <= i_span_end:
                        if i != i_best:
                            # we could use some other error code
                            angle_limits[i] = 0.0
                        i += 1
            i_span_start = i_span_end + 1
    # End angle limit cleaning!

    for i in range(len(angle_limits)):
        if angle_limits[i] > angle_limit:
            corners.append(i)

# end corner detection
# --------------------


def calc_spline(
        points: Sequence[Vector],
        *,
        error: float,
        use_detect_corners: bool=True,
        detect_corners_angle_threshold: float=math.radians(100)
        ) -> List[Cubic]:

    """
    Main function:

    Take an array of 3d points.
    return the cubic splines
    """

    # points = [tuple(p) for p in points]

    corners = [0]
    if use_detect_corners and len(points) > 2:
        corners_detect(
                points,
                corners,
                radius_min=3.0,
                radius_max=10.0,
                # arbitrary, avoid hanging when radius_min is too big.
                samples_max=32,
                angle_limit=detect_corners_angle_threshold,
                )

    corners.append(len(points) - 1)

    r_cubic_array = []

    for i in range(1, len(corners)):
        points_offset_len = corners[i] - corners[i - 1] + 1
        first_point = corners[i - 1]

        # print(points_offset_len, first_point)
        assert(points_offset_len >= 1)
        if points_offset_len > 1:
            tan_l = normalized_vnvn(
                    points[first_point],
                    points[first_point + 1])
            tan_r = normalized_vnvn(
                    points[first_point + points_offset_len - 2],
                    points[first_point + points_offset_len - 1])

            fit_cubic_to_points(
                    points[first_point:], points_offset_len,
                    tan_l, tan_r, error, r_cubic_array)

        elif len(points) == 1:
            assert(points_offset_len == 1)
            assert(len(corners) == 2)
            assert(corners[0] == 0)
            assert(corners[1] == 0)
            p = points[0]
            r_cubic_array.append(Cubic(p, p, p, p))

    print(
        "points:", len(r_cubic_array),
        ", corners:", len(corners) - 2,
        ", uuid:", hex(hash(tuple(r_cubic_array)) % 0xFFFFFFFF)[2:].upper(),
        )

    return r_cubic_array


# ----------------------------------------------------------------------------
# Addon

import bpy
import bgl
from bpy.props import (
        BoolProperty,
        FloatProperty,
        PointerProperty,
        )


def context_cursor(context):
    scene = context.scene
    space = context.space_data

    cursor = (space if space and space.type == 'VIEW_3D'
              else scene).cursor_location
    return cursor


def points_to_spline(
        spline: bpy.types.Spline,
        spline_data: List[Cubic],
        radius_min: float,
        radius_max: float,
        ) -> None:

    spline.bezier_points.add(len(spline_data))
    bezier_points = spline.bezier_points

    use_radius = len(spline_data[0][0]) > 3

    if use_radius:
        radius_min = min(radius_min, radius_max)
        radius_max = max(radius_min, radius_max)
        radius_range = radius_max - radius_min

    i = 0
    for i in range(len(bezier_points) - 1):
        cubic = spline_data[i]

        bezt_a = bezier_points[i]
        bezt_b = bezier_points[i + 1]

        bezt_a.co[0:3] = cubic[0][:3]
        bezt_a.handle_right = cubic[1][:3]
        bezt_b.handle_left = cubic[2][:3]
        bezt_b.co[0:3] = cubic[3][:3]

        if use_radius:
            bezt_a.radius = radius_min + (cubic[0][3] * radius_range)

        i += 1

    # cleanup first/last points
    bezt = bezier_points[0]
    bezt.handle_left = bezt.co + (bezt.co - bezt.handle_right)

    bezt = bezier_points[-1]
    bezt.handle_right = bezt.co + (bezt.co - bezt.handle_left)

    if use_radius:
        bezt.radius = radius_min + (spline_data[-1][3][3] * radius_range)


def mouse_path_to_spline(
        context,
        mouse_path: list,
        ) -> None:

    curve_freehand = context.scene.curve_freehand

    spline_data = calc_spline(
            mouse_path,
            error=curve_freehand.error_threshold,
            use_detect_corners=curve_freehand.use_detect_corners,
            detect_corners_angle_threshold=curve_freehand.detect_corners_angle_threshold,
            )

    obj = context.object
    curve = obj.data
    matrix_inverse = obj.matrix_world.inverted()

    from bpy_extras.view3d_utils import region_2d_to_location_3d
    region = context.region
    rv3d = context.region_data
    depth_pt = context_cursor(context)

    spline_data = [
            Cubic(*[
                (*(matrix_inverse * region_2d_to_location_3d(region, rv3d, pt, depth_pt)), *pt[2:])
                 for pt in cubic])
            for cubic in spline_data]

    bpy.ops.curve.select_all(action='DESELECT')

    spline = curve.splines.new(type='BEZIER')
    points_to_spline(
            spline, spline_data,
            curve_freehand.pressure_min,
            curve_freehand.pressure_max,
            )

    for bezt in spline.bezier_points:
        bezt.select_control_point = True
        bezt.select_left_handle = True
        bezt.select_right_handle = True

        # we could get this from the calculation...
        a, b, c = bezt.handle_left, bezt.co, bezt.handle_right
        if (a - b).angle(b - c, -1.0) < 1e-2:
            bezt.handle_left_type = bezt.handle_right_type = 'ALIGNED'


def draw_callback_px(self, context):
    bgl.glEnable(bgl.GL_BLEND)
    bgl.glColor4f(0.0, 0.0, 0.0, 0.5)
    bgl.glLineWidth(2)

    if context.scene.curve_freehand.use_pressure:
        pt_a = self.mouse_path[0]
        for i in range(1, len(self.mouse_path)):
            # weak but good enough for preview
            pt_b = self.mouse_path[i]
            bgl.glPointSize(1 + (pt_a[2] * 40.0))
            bgl.glBegin(bgl.GL_POINTS)
            bgl.glVertex2i(pt_a[0], pt_a[1])
            bgl.glVertex2i(pt_b[0], pt_b[1])
            bgl.glEnd()
            pt_a = pt_b
    else:
        bgl.glBegin(bgl.GL_LINE_STRIP)
        for pt in self.mouse_path:
            bgl.glVertex2i(pt[0], pt[1])
        bgl.glEnd()

    bgl.glLineWidth(1)
    bgl.glDisable(bgl.GL_BLEND)
    bgl.glColor4f(0.0, 0.0, 0.0, 1.0)


class CurveFreehandDraw(bpy.types.Operator):
    """Draw curve splines in edit-mode"""
    bl_idname = "curve.draw"
    bl_label = "Curve Draw Tool"

    bl_options = {'UNDO'}

    def modal(self, context, event):
        context.area.tag_redraw()
        if event.type in {'MOUSEMOVE', 'INBETWEEN_MOUSEMOVE'}:
            mval = (event.mouse_region_x, event.mouse_region_y)

            if context.scene.curve_freehand.use_pressure:
                print(event.pressure)
                mval = mval + (event.pressure,)

            # don't add doubles
            if not self.mouse_path or mval != self.mouse_path[-1]:
                self.mouse_path.append(mval)

        elif event.type == self.init_event_type:
            bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')

            if len(self.mouse_path) > 1:
                mouse_path_to_spline(context, self.mouse_path)
                return {'FINISHED'}
            else:
                return {'CANCELLED'}

        elif event.type == 'ESC':
            bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):

        self.init_event_type = event.type

        # Check object context
        if context.mode == 'EDIT_CURVE':
            pass
        else:
            self.report({'WARNING'}, "Edit-Curve mode not available")
            return {'CANCELLED'}

        # Check view context
        if context.area.type == 'VIEW_3D':
            args = (self, context)
            self._handle = bpy.types.SpaceView3D.draw_handler_add(draw_callback_px, args, 'WINDOW', 'POST_PIXEL')

            self.mouse_path = []

            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}


class VIEW3D_PT_tools_curveedit_options(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    bl_category = "Options"
    bl_context = "curve_edit"
    bl_label = "Curve Options"

    @classmethod
    def poll(cls, context):
        return context.active_object

    def draw(self, context):
        layout = self.layout

        scene = context.scene
        curve_freehand = scene.curve_freehand

        col = layout.label("Freehand Drawing:")
        col = layout.column()
        col.prop(curve_freehand, "error_threshold")

        col.prop(curve_freehand, "use_detect_corners")
        colsub = col.column()
        colsub.active = curve_freehand.use_detect_corners
        colsub.prop(curve_freehand, "detect_corners_angle_threshold")

        col.prop(curve_freehand, "use_pressure")
        colsub = col.column(align=True)
        colsub.active = curve_freehand.use_pressure
        colsub.prop(curve_freehand, "pressure_min", text="Min")
        colsub.prop(curve_freehand, "pressure_max", text="Max")


class CurveFreehandProps(bpy.types.PropertyGroup):
    use_detect_corners = BoolProperty(
            name="Detect Corners",
            description="Detect sharp corners",
            default=True,
            )

    error_threshold = FloatProperty(
            name="Detail",
            description="Detail threshold (smaller values give more detailed splines)",
            subtype='PIXEL',
            min=0.01, max=1000.0,
            default=10.0,
            )

    detect_corners_angle_threshold = FloatProperty(
            name="Angle Limit",
            description="Detail threshold (smaller values give more detailed splines)",
            subtype='ANGLE',
            min=0.0, max=math.pi,
            default=math.radians(100),
            )

    use_pressure = BoolProperty(
            name="Tablet Pressure",
            description="Map tablet pressure to radius",
            default=False,
            )
    pressure_min = FloatProperty(
            name="Pressure Min",
            min=0.0, max=100.0,
            default=0.0,
            )
    pressure_max = FloatProperty(
            name="Pressure Max",
            min=0.0, max=100.0,
            default=1.0,
            )


classes = (
    CurveFreehandDraw,
    CurveFreehandProps,
    VIEW3D_PT_tools_curveedit_options,
    )

addon_keymaps = []


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.curve_freehand = PointerProperty(type=CurveFreehandProps)

    kc = bpy.context.window_manager.keyconfigs.addon

    if kc:  # don't register keymaps from command line
        km = kc.keymaps.new(name="Curve", space_type='EMPTY', region_type='WINDOW')
        kmi = km.keymap_items.new(CurveFreehandDraw.bl_idname, 'ACTIONMOUSE', 'PRESS', shift=True)
        kmi.active = True
        addon_keymaps.append((km, kmi))


def unregister():
    del bpy.types.Scene.curve_freehand

    for cls in classes:
        bpy.utils.unregister_class(cls)

    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()


if __name__ == "__main__":
    register()


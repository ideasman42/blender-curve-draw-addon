
*****************************
Freehand Curve Drawing Add-On
*****************************

This add-on is a port from OpenToonz freehand drawing into Blender.


To use this, download the repository as a zipfile and install from within Blender.

Add a curve in edit-mode and ``Shift-LMB`` to draw.

Options for this tool can be adjusted in the *Options* tab.


Details
=======

Currently the handles are calculated in screenspace, however this funciton can operate on 3D splines too.

This may be eventually added into Blender (ported to C).


Changes from OpenToonz
======================

- Support n-dimensions (can be used for calculating a 3D curve & pressure... or other custom-data).
- Replace 2D bound-box handle clamping with vector length clamping.

